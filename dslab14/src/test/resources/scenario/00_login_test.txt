*	CloudController	controller
*	Client			alice
*	Node			node1
*	Node			node2
*	Node			node3
*	Node			node4
*	Client			bill

alice:		!credits

alice:		!login alice 12345

alice:		!credits

bill:		!credits

bill:		!login bill 23456

controller:	!users

controller:	!nodes

bill:		!compute 1 + 1 + 6

controller:	!nodes

bill: 		!compute 1 - 1 - 6

controller:	!nodes

alice:		!compute 1 + 100 * 4

controller:	!nodes

bill: 		!compute 1 / 1 * 6

controller:	!nodes

alice:		!compute 1 - 100 * 4

controller: !users

controller:	!nodes

controller: !exit
alice:		!exit
node1:		!exit
node2:		!exit
bill:		!exit
