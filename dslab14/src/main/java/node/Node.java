package node;

import util.Config;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import client.EncryptionManager;
import controller.CloudController;
import myClasses.CloudControllerThreadForNode;
import myClasses.CloudControllerThreadPool;
import myClasses.CloudControllerUDPThreadPool;
import myClasses.NodeTCPThreadPool;
import myClasses.ServiceNode;
import myClasses.TimerTaskAlive;
import myClasses.TimerTaskSendAlive;

public class Node implements INodeCli, Runnable {

	private static String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	public static boolean exit_node = false;
	private BufferedOutputStream outtoserver;
	private static String arguments;
	private Timer timer = new Timer();
	private DatagramSocket udpSocket;
	public static ExecutorService nodeThreadPoolTCP = Executors
			.newCachedThreadPool();
	public static Map<Integer, String> finishedExpressions = new HashMap<Integer, String>();
	public static int rmin;
	public static boolean helloMessage = false;
	public static boolean aliveMessage = false;
	public static boolean firstNode = false;
	public static String resource = "0";
	public static String oldResource = "0";
	public static String sharedResource = "0";
	private String directory; 
	
	// SimpleDateFormat is not thread-safe, so give one to each thread
    private static final ThreadLocal<SimpleDateFormat> formatter = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue()
        {
            return new SimpleDateFormat("yyyyMMdd_HHmmss.SSS");
        }
        
    };
	
	/**
	 * Create Client TCP-Socket
	 */
	private ServerSocket node = null;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Node(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;

		// TODO
	}

	@Override
	public void run() {
		// TODO
		//TCP 16500 UDP 16501
		
			try {
				
				node = new ServerSocket(config.getInt("tcp.port"));
				arguments = config.getString("node.operators");
				udpSocket = new DatagramSocket();
				rmin = config.getInt("node.rmin");
				directory = config.getString("log.dir");
			} catch (IOException e) {
				throw new RuntimeException("Cannot listen on TCP/UDP port.", e);
			}
			
		if (udpSocket == null || node == null) {
			nodeThreadPoolTCP.shutdown();
			timer.cancel();
		} else {

			try {
				
				/**
				 * erstelle neuen Server-Thread
				 */
				// handle incoming connections from client in a separate thread
				nodeThreadPoolTCP.execute(new NodeTCPThreadPool(node, rmin,directory, new EncryptionManager(config)));
				
				// erstelle einen timer welcher alle node.alive zeiten ein
				// isalive packt sendet
				//InetAdress = localhost/127.0.0.1
				//starte nur wenn hellomessage und alivemessage true sind
					//System.out.println("beginne mit timertask alive messages");
					timer.schedule(
							new TimerTaskSendAlive(udpSocket, config
									.getInt("tcp.port"), config.getString("controller.host"),
									config.getInt("controller.udp.port"), rmin),
							new Date() , config.getInt("node.alive"));
				
				
				BufferedReader inFromStdin = new BufferedReader(
						new InputStreamReader(userRequestStream));


				String in = "";
				String[] split;

				//System.out.println(udpSocket.getInetAddress());
				/*System.out.println("Node Online");
				System.out.println("Localport" + node.getLocalPort()
						+ " InetAdress " + node.getInetAddress()
						+ " Localsocketadress " + node.getLocalSocketAddress());*/

				while (exit_node == false
						&& (in = inFromStdin.readLine()) != null) {
					in.replaceFirst("\n", " ");
					split = in.split(" ");

					if (split[0].equals("!exit")) {
						exit_node = true;
						exit();
						break;
					} else if (split[0].equals("!resources")) {
						System.out.println(resources());
					} else if (split[0].equals("!test"))	{
						
						System.out.println(history(10));
						
					} else {
						System.out
								.println("Invalid Command. Use following commands instead:");
						System.out.println("!exit");
						System.out.println("!resources");
					}
				}

			} catch (UnknownHostException e) {
				System.err.println("Node closed"); // : Fehler beim Erstellen
													// des
													// Client Sockets");
			} catch (IOException e) {
				// System.err.println("Client: IOException");
			} 
		}
		
	}
	
	public static String getArguments() {
		return arguments;
	}
	
	public static String getComponentName() {
		return componentName;
	}
	
	//public static String callHistory(int number) throws IOException {
	//	return history(number);
	//}

	@Override
	public String exit() throws IOException {
		String out = "node shut down.";
		if (node != null) {
			exit_node = true;
			nodeThreadPoolTCP.shutdown();
			timer.cancel();
			node.close();
		} else {
			exit_node = true;
		}
		return out;
	}

	public String history(int numberOfRequests) throws IOException {
		// TODO Auto-generated method stub
		int count = numberOfRequests;
		int counter = 0;
		String result = "";
		if (finishedExpressions.size() > 0) {
			for(String line : finishedExpressions.values()) {
				counter ++;
				result += line + "\n";
				if (counter == count) {
					break;
				}
			}
		} else {
			result = "There are no expressions at the moment.";
		}
		return result;
	}
	
	
	public static String loggerFacility(String node, String input, String output) {
		//System.out.println(formatter.get().format(new Date()));
		String result = "Information: Log successfully written.";
		synchronized (result) {
			// erstelle byte array
			String folder = "log/" + node + "/";
			String fileEnd = "_" + node + ".log";
			Path path = FileSystems.getDefault().getPath(folder,
					formatter.get().format(new Date()) + fileEnd);
			String s = input + "\n" + output;
			byte data[] = s.getBytes();

			try  {
				
				OutputStream out = new BufferedOutputStream(
						Files.newOutputStream(path));

				out.write(data, 0, data.length);
				out.close();
			} catch (IOException x) {
				System.err.println(x);
				result = "Error!";
			}

			return result;
		}
		
	}
	

	/**
	 * @param args
	 *            the first argument is the name of the {@link Node} component,
	 *            which also represents the name of the configuration
	 */
	public static void main(String[] args) {
		Node node = new Node(args[0], new Config(args[0]), System.in,
				System.out);
		// TODO: start the node
		node.run();
	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String resources() throws IOException {
		// TODO Auto-generated method stub
		return resource + " alte ressources: " + oldResource;
	}

}
