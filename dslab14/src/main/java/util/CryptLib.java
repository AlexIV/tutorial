package util;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;


public class CryptLib {

	private SecretKey secretKey = null;
	private IvParameterSpec iv = null;
	private static final String ALGORITHM = "RSA/NONE/OAEPWithSHA256AndMGF1Padding";
	private static final String ALGORITHMAES = "AES/CTR/NoPadding";

	public SecretKey getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(SecretKey secretKey) {
		this.secretKey = secretKey;
	}

	public IvParameterSpec getIv() {
		return iv;
	}

	public void setIv(IvParameterSpec iv) {
		this.iv = iv;
	}

	public String decryptRSA(byte[] text, PrivateKey key) {
		byte[] plaintext = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, key);
			plaintext = cipher.doFinal(text);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return new String(plaintext);
	}

	public byte[] encryptRSA(String text, PublicKey key) {
		byte[] cipherText = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			System.out.println("Error while encrypting " + e.getMessage());
		}
		return cipherText;
	}

	public byte[] cryptAES(byte[] msg, int mode) {
		assert secretKey != null && iv != null : "SecretKey not properly initialized";
		try {
			Cipher crypt = Cipher.getInstance(ALGORITHMAES);
			crypt.init(mode, secretKey, iv);
			return crypt.doFinal(msg);
		} catch (Exception e) {
			System.out.println("Error while encrypting " + e.getMessage());
		}
		return null;
	}

	public byte[] generateSecureRandom(int length) {
		SecureRandom secureRandom = new SecureRandom();
		final byte[] number = new byte[length];
		secureRandom.nextBytes(number);
		return number;
	}

	public SecretKey generateAESSecretKey() throws NoSuchAlgorithmException {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(256);
		return generator.generateKey();
	}
}
