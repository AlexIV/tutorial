package admin;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

import admin.INotificationCallback;

public class Admin{

		private INotificationCallback callback;
		private HashMap<String,ArrayList<Integer>> list;

		public Admin(String name, int zahl, INotificationCallback callback ){
			list = new HashMap<String, ArrayList<Integer>>();
			this.callback=callback;

			list.put(name, new ArrayList<Integer>());
			
			list.get(name).add(zahl);
		}

		
		public HashMap<String, ArrayList<Integer>> getList() {
			return list;
		}

		public void setList(HashMap<String, ArrayList<Integer>> list) {
			this.list = list;
		}

		public void setCallback(INotificationCallback callback) {
			this.callback = callback;
		}
		
		public INotificationCallback getCallback() {
			return this.callback;
		}
		
		public boolean add(String username, int credits) {
			synchronized(list){
				if(!list.containsKey(username))
					list.put(username, new ArrayList<Integer>());

				if(!list.get(username).contains(credits)){
					return list.get(username).add(credits);
				}
				return false;
			}
		}

		public void checkCredits(String user, int zahl) throws RemoteException {
           
			synchronized(list){	
				if(!list.containsKey(user)){
					list.put(user, new ArrayList<Integer>());
				}
				for(Integer credits : list.get(user)){
					if(zahl < credits){
						callback.notify(user, credits);
					}
				}

			}
		}

	}


