package admin;

import java.io.PrintStream;
import java.io.Serializable;
import java.rmi.RemoteException;

public class NotificationCallback implements INotificationCallback, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void notify(String username, int credits) throws RemoteException {
		// TODO Auto-generated method stub
		System.out.println("Notification: " + username + " has less than " + credits + " credits.");
	   // this.userResponseStream.print("Notification: " + username + " has less than " + credits + " credits.");
	
	} 

}
