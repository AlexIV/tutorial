package admin;

import controller.IAdminConsole;
import model.ComputationRequestInfo;
import util.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Serializable;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.Key;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Comparator;

/**
 * Please note that this class is not needed for Lab 1, but will later be
 * used in Lab 2. Hence, you do not have to implement it for the first
 * submission.
 */
public class AdminConsole implements IAdminConsole, INotificationCallback, Runnable{

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private String bindingName; 
	private String controllerHost; 
	private int controllerRMIPort; 
	private String adminKeys;
	private Config configAdmin;
	private Registry registry;
	private IAdminConsole remote;
	private INotificationCallback callback;
	private boolean call;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public AdminConsole(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
        this.callback = new NotificationCallback();
        this.call = true;
		// TODO
	}

	@Override
	public void run() {
		// TODO
		this.bindingName = config.getString("binding.name");
		this.controllerHost = config.getString("controller.host");
		this.controllerRMIPort = config.getInt("controller.rmi.port");
		this.adminKeys = config.getString("keys.dir");

	    Registry registry;
	  
		try {
			registry = LocateRegistry.getRegistry(controllerHost,controllerRMIPort );
		    remote = (IAdminConsole) registry.lookup(this.bindingName);
		    UnicastRemoteObject.exportObject(this, 0);
		   //remote.statistics 
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BufferedReader userInputReader = null;
		try {
			// create reader to read from console
			userInputReader = new BufferedReader(new InputStreamReader(
					this.userRequestStream));

			System.out.println("Admin is up! Enter command.");

			while (true) {
				// read input from console
				String input = userInputReader.readLine();
                // System.out.println(input);
				// in case "!stop" was entered (or the end of the stream has
				// been reached) shut down client
				if (input == null || input.startsWith("!exit")) {
					
					try {
						// unexport the previously exported remote object
						UnicastRemoteObject.unexportObject(this, true);
						System.out.println("Admin is closed");
					} catch (NoSuchObjectException e) {
						System.err.println("Error while unexporting object: "
								+ e.getMessage());
					}			
					
					//userInputReader.close();
					break;
				} else if (input.startsWith("!statistics")) {

					LinkedHashMap<Character, Long> stats =  remote.statistics();
					if(stats.isEmpty()){
						System.out.println("No statistics available");
					}else{
					for (Map.Entry<Character, Long> entry : stats.entrySet()) {
				     System.out.println(entry.getKey() + " " + entry.getValue());
	
					}
					
					}

				}else if (input.startsWith("!subscribe")) {

					String[] split;
					
					split = input.split(" ");
					int tmp = new Integer(split[2]);
					
					if(remote.subscribe(split[1],tmp,this)){
						
						System.out.println("Succesfully subscribed for user " + split[1]);
					}else{
						
						System.out.println("Error by subscribed for user " + split[1]);
					}
					
					
				} else if (input.startsWith("!getLogs")) {

					List<ComputationRequestInfo> logs =  remote.getLogs();
                    //System.out.println(logs);
					
					if(logs.isEmpty()){
						System.out.println("No Logs available");
					}else{
					for( ComputationRequestInfo cri : logs){
						System.out.println(cri.toString());
					}
					}
				}else {
					System.out.println("Command not known!");
				}

			}
		} catch (RemoteException e) {
			System.out
					.println("An error occurred while communicating with the server: "
							+ e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getClass() + ": " + e.getMessage());
		} finally {

			if (userInputReader != null)
				try {
					userInputReader.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
		}
		
		
		

	}

	@Override
	public boolean subscribe(String username, int credits,
			INotificationCallback callback) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<ComputationRequestInfo> getLogs() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkedHashMap<Character, Long> statistics() throws RemoteException {
		// TODO Auto-generated method stub
	 return null;
	}

	@Override
	public Key getControllerPublicKey() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUserPublicKey(String username, byte[] key)
			throws RemoteException {
		// TODO Auto-generated method stub
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link AdminConsole}
	 *            component
	 */
	public static void main(String[] args) {
		AdminConsole adminConsole = new AdminConsole(args[0], new Config(
				"admin"), System.in, System.out);
		adminConsole.run();
	}

	@Override
	public void notify(String username, int credits) throws RemoteException {
		// TODO Auto-generated method stub
		System.out.println("Notification: " + username + " has less than " + credits + " credits.");
	}


 
	
	
}
