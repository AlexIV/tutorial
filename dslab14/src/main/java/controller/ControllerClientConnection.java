package controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;

import myClasses.User;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;

import util.Config;
import util.CryptLib;
import util.Keys;

public class ControllerClientConnection {

	private CryptLib crypt;
	private byte[] controllerChallengeB64;
	private String keysDir;
	private String controllerKey;
	private boolean isValid = false;
	private String username;
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public ControllerClientConnection(Config config) {
		crypt = new CryptLib();
		controllerKey=config.getString("key");
		keysDir=config.getString("keys.dir");
	}

	public String loginHandshakeMsg1(String msg1) throws IOException,
			NoSuchAlgorithmException {
		PrivateKey privateKey = getPrivateControllerKey();
		String deCryptMsg1 = crypt.decryptRSA(Base64.decode(msg1), privateKey);
		String[] arguments = deCryptMsg1.split("\\s");
		username = arguments[1];

			controllerChallengeB64 = Base64.encode(crypt
					.generateSecureRandom(32));
			byte[] iv = crypt.generateSecureRandom(16);
			crypt.setIv(new IvParameterSpec(iv));
			crypt.setSecretKey(crypt.generateAESSecretKey());
			return new String(Base64.encode(crypt.encryptRSA(
					"!ok "
							+ arguments[2]
							+ " "
							+ new String(controllerChallengeB64)
							+ " "
							+ new String(Base64.encode(crypt.getSecretKey()
									.getEncoded())) + " "
							+ new String(Base64.encode(iv)),
					getUsersPublicKey(username))));
	}

	private PrivateKey getPrivateControllerKey() throws IOException {
		String pathToPrivateKey = controllerKey;
		return Keys.readPrivatePEM(new File(pathToPrivateKey));
	}

	private PublicKey getUsersPublicKey(String username) throws IOException {
		String path = keysDir + "/" + username + ".pub.pem";
		PEMReader in = new PEMReader(new FileReader(path));
		return (PublicKey) in.readObject();
	}

	public void loginHandshakeMsg3(String msg3) throws IOException {
		String pcB64 = new String(crypt.cryptAES(
				Base64.decode(msg3.getBytes()), Cipher.DECRYPT_MODE));
		if (new String(controllerChallengeB64).equals(pcB64)) {
			isValid = true;
		} else {
			isValid = false;
		}
	}

	public byte[] decrypt(String requestE) {
		byte[] msg = Base64.decode(requestE);
		return Base64.decode(crypt.cryptAES(msg, Cipher.DECRYPT_MODE));
	}

	public String encrypt(String obj) throws IOException {
		byte[] msg = Base64.encode(obj.getBytes());
		return new String(Base64.encode(crypt
				.cryptAES(msg, Cipher.ENCRYPT_MODE)));
	}
}
