package controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.Key;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import admin.INotificationCallback;
import model.ComputationRequestInfo;
import myClasses.CloudControllerThreadPool;
import myClasses.CloudControllerUDPThreadPool;
import myClasses.ServiceNode;
import myClasses.TimerTaskAlive;
import myClasses.User;
import util.Config;
import admin.Admin;

public class CloudController implements ICloudControllerCli,IAdminConsole,Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	public static Map<String, User> loggedInUsers = new HashMap<String, User>();
	private ServerSocket serverSocket;
	public static ExecutorService threadPoolTCP = Executors
			.newCachedThreadPool();
	public static ExecutorService threadPoolUDP = Executors
			.newCachedThreadPool();
	public static boolean exit1 = false;
	private Timer timer = new Timer();
	private DatagramSocket udpSocket;
	public static Map<String, ServiceNode> handeltNodes = new HashMap<String, ServiceNode>();
	private String bindingName; 
	private String controllerHost; 
	private int controllerRMIPort; 
	private String adminKeys;
	public static int rmax;
	private Registry registry;
    private CloudControllerThreadPool cctp;
    private static List<Admin> admins;
	// public static Map<String, ServiceNode> computedNodes = new
	// HashMap<String, ServiceNode>();

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public CloudController(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		admins = new ArrayList<Admin>();
      
	}

	@Override
	public void run() {
		// create and start a new TCP ServerSocket
		try {
			serverSocket = new ServerSocket(config.getInt("tcp.port"));
			udpSocket = new DatagramSocket(config.getInt("udp.port"));
			this.bindingName = config.getString("binding.name"); // oder config 
			this.controllerHost = config.getString("controller.host");
			this.controllerRMIPort = config.getInt("controller.rmi.port");
			this.adminKeys = config.getString("keys.dir");
			this.rmax = config.getInt("controller.rmax");
		
		} catch (IOException e) {
			throw new RuntimeException("Cannot listen on TCP/UDP port.", e);
		}

		if (serverSocket == null) {
			threadPoolTCP.shutdown();
			threadPoolUDP.shutdown();
			timer.cancel();
		} else {

			/**
			 * erstelle neuen Server-Thread
			 */
			// handle incoming connections from client in a separate thread
	        cctp = new CloudControllerThreadPool(serverSocket,
	        							new Config("user"),config);
			threadPoolTCP.execute(cctp);
			// listen to UDP thread
			threadPoolUDP.execute(new CloudControllerUDPThreadPool(udpSocket));
			// erstelle einen timer welcher alls node.checkPeriod zeiten die
			// verfuegbaren nodes kontroliert
			timer.schedule(new TimerTaskAlive(), new Date(),
					config.getInt("node.checkPeriod"));

			// System.out.println("Server is up and running!");
			// System.out.println("Localport" + serverSocket.getLocalPort() +
			// " InetAdress " + serverSocket.getInetAddress()
			// + " Localsocketadress " + serverSocket.getLocalSocketAddress() +
			// " " + serverSocket.toString());

			// RMI BEGINN
			try {

				registry = LocateRegistry.getRegistry(controllerHost, controllerRMIPort);				
				// Exception, wenn keine registry vorhanden ist
				registry.list();			
				System.out.println("RMI-Registry started");

			}catch (RemoteException e) {

				try {
					// create and export the registry instance on localhost at the
					// specified port
					registry = LocateRegistry.createRegistry(controllerRMIPort);

					// create a remote object of this server object
					IAdminConsole remote = (IAdminConsole) UnicastRemoteObject
							.exportObject(this, 0);
					// bind the obtained remote object on specified binding name in the
					// registry
					registry.bind(bindingName, remote);
					
				} catch (RemoteException ex) {
					throw new RuntimeException("Error while starting server.", e);
				} catch (AlreadyBoundException ex) {
					throw new RuntimeException(
							"Error while binding remote object to registry.", e);
				}

			}
			
			//RMI ENDE

			// create a reader to read from the console
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					userRequestStream));
			
			getAllUsers(new Config("user"));

			String in;
			boolean exit = false;
			while (exit == false) {
				try {
					// read commands from the console
					// reader.readLine();
					while ((in = reader.readLine()) != null && exit == false) {
						if (in.startsWith("!exit")) {
							System.out.println(exit());
							exit = true;
							break;
							// alice online Credits: 500
						} else if (in.startsWith("!users")) {
							System.out.println(users());
						} else if (in.startsWith("!nodes")) {
							System.out.println(nodes());
						} else {
							System.out
									.println("Unknown command. Please use one of the following comands: !exit, !users, !nodes.");
						}
					}

				} catch (IOException e) {
					// IOException from System.in is very very unlikely (or
					// impossible)
					// and cannot be handled
					System.out.println("Server closed");// : Fehler beim
														// Erstellen/Schlie�en
														// des Sockets");
				}
			}

		}
		
		try {
			// unexport the previously exported remote object
			UnicastRemoteObject.unexportObject(this, true);
		} catch (NoSuchObjectException e) {
			System.err.println("Error while unexporting object: "
					+ e.getMessage());
		}

		try {
			// unbind the remote object so that a client can't find it anymore
			registry.unbind(config.getString("binding.name"));
		} catch (Exception e) {
			System.err.println("Error while unbinding object: "
					+ e.getMessage());
		}

	}

	@Override
	public String nodes() throws IOException {
		// TODO Auto-generated method stub
		String allNodes = "";
		for (ServiceNode node : handeltNodes.values()) {
			// allNodes = allNodes + node.getName() + " " + node.getStatus() +
			// " " + node.getEnd() + " " + node.getAdress() + " " +
			// node.getPort() + " " + node.getSocket();
			allNodes = allNodes + "IP: " + node.getAdress() + " Port: "
					+ node.getSocket() + " " + node.getStatus() + " "
					+ "Usage: " + node.getUsage() + "\n";
		}
		return allNodes;
	}
	
	private boolean getAllUsers(Config config) {
		String[] temp;
		Set setA = new HashSet(); 
		setA = config.listKeys();
		Iterator iterator = setA.iterator();
		
		while(iterator.hasNext()) {
		  String element = (String) iterator.next();
		  temp = element.split("\\.");
		  if(temp.length == 2) {
			  if(temp[1].equals("credits")) {
				  loggedInUsers.put(temp[0], new User(temp[0], null, config.getInt(element), "offline"));
			  }
		  }
		}
		return true;
	}

	@Override
	public String users() throws IOException {
		// TODO Auto-generated method stub
		// alice online Credits: 500
		String allUsers = "";
		for (User u : loggedInUsers.values()) {
			allUsers = allUsers + u.getName() + " " + u.getStatus() + " "
					+ "Credits:" + " " + u.getCredits() + "\n";
		}
		return allUsers;
	}
   
	@Override
	public String exit() throws IOException {
		// TODO Auto-generated method stub
		String out = "CloudControll shut down.";
		// close socket and listening thread
		if (serverSocket != null) {
			exit1 = true;
			for (User u : loggedInUsers.values()) {
				u.setStatus("offline");
				u.getSocket().close();
				// System.out.println(u.getSocket() + " geschlossen!!!!");
			}
			

			
			timer.cancel();
			serverSocket.close();
			udpSocket.close();
			threadPoolTCP.shutdownNow();
			threadPoolUDP.shutdownNow();
		}

		return out;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link CloudController}
	 *            component
	 */
	public static void main(String[] args) {
		CloudController cloudController = new CloudController(args[0],
				new Config("controller"), System.in, System.out);

		// TODO: start the cloud controller
		cloudController.run();
	}

	@Override
	public boolean subscribe(String username, int credits,
			INotificationCallback callback) throws RemoteException {
		// TODO Auto-generated method stub
		boolean subscribe = true; 
		for (User u : loggedInUsers.values()) {
			if( u.getName().equals(username)){
				subscribe = true; 
			}
		}
		// Wenn Name nicht vorhanden ist 
		if (!subscribe){
			return false; 
		}		
	//	cctp.addSubscribes(username, credits, callback);		
		
		for(Admin a : admins){
			   if(a.getCallback().equals(callback))
			    if(a.add(username,credits)){
			     checkCredits();
			     return true;
			    }else
			     return false;
			  }
		// Neuer Admin wird hinzugefügt 
			  Admin a = new Admin(username, credits,callback);			  
			  admins.add(a);
			  checkCredits();
			  return true;
			 
		
		
		
	}

	@Override
	public List<ComputationRequestInfo> getLogs() throws RemoteException {
		
		Socket nSocket = null;
		BufferedReader reader1 = null;
		PrintWriter pw1 = null;
		List<ComputationRequestInfo> list = new ArrayList<ComputationRequestInfo>();
		//System.out.println("ss");
		for(ServiceNode sn : handeltNodes.values()){
			if(sn.getStatus() == "online"){

				try {
					 nSocket = new Socket(sn.getAdress(), sn.getSocket());

					 pw1 = new PrintWriter(
							nSocket.getOutputStream(), true);  

					pw1.println("!getLogs");
					pw1.flush();
					
                    
					 reader1 = new BufferedReader(new InputStreamReader(
							nSocket.getInputStream()));			
					//String line=reader1.readLine();

					String line = null;
					 while ((line = reader1.readLine()) != null) { 
					// while (line != null) {
					//	 System.out.println(line);
				 //   for (String line = reader1.readLine(); line != ""; line = reader1.readLine()) {
					
		                 if(line.contains("EOF")){
		                	 break;
		                 }
						 
						 ComputationRequestInfo cri = new ComputationRequestInfo();						 
						 String[] split = line.split(" ");					 
						 if(split.length == 7 ){
						 
						 cri.setTime(line.substring(0,19));
					     cri.setNode(line.substring(20,25));
						 cri.setRequest(split[2] + " " + split[3] + " " + split[4]);
						 cri.setResult(split[6]);
		
						 list.add(cri);
						// System.out.println(list);		 
					 }
				   				
					//String op= reader1.readLine();		
					//System.out.println(op);							
		//			nodes = new Socket(sn.getAdress(), sn.getPort());
		//			BufferedReader inNodes = new BufferedReader(
		//					new InputStreamReader(nodes.getInputStream()));
		//			BufferedOutputStream outNodes = new BufferedOutputStream(
		//					new DataOutputStream(nodes.getOutputStream()));
		//	   	   String logs = "!logs";
		//		   outNodes.write(logs.getBytes());		 
						//  line = reader1.readLine();
						 }
					 
					 
					 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				finally{
					
					try {
						nSocket.close();
						reader1.close();	 
					    pw1.close();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					
				}

				
			}

			
		}
		Collections.sort(list, new Comparator<ComputationRequestInfo>() {
	        @Override
	        public int compare(ComputationRequestInfo cri1,ComputationRequestInfo  cri2)
	        {

	            return  cri1.getTime().compareTo(cri2.getTime());
	        }
	    });
		
		
		return list;
	}

	@Override
	public LinkedHashMap<Character, Long> statistics() throws RemoteException {
	//Long a = (long) 20; 
		
		Map<Character, Long> statistics = new LinkedHashMap<Character, Long>();
		if(cctp.getPlus()> 0){
		statistics.put('+', new Long(cctp.getPlus()));
		}
		if(cctp.getDiv()>0){
		statistics.put('/', new Long(cctp.getDiv()));
		}
		if(cctp.getMinus()>0){
		statistics.put('-', new Long(cctp.getMinus()));
		}
		if(cctp.getMal()>0){
		statistics.put('*', new Long(cctp.getMal()));
		}
		Map<Character, Long> sortedMap = sortByComparator(statistics);
		return (LinkedHashMap<Character, Long>) sortedMap;
		
	}

	@Override
	public Key getControllerPublicKey() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUserPublicKey(String username, byte[] key)
			throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	
	
	private static Map<Character, Long> sortByComparator(Map<Character, Long> unsortMap) {
		 
		// Convert Map to List
		List<Map.Entry<Character, Long>> list = 
			new LinkedList<Map.Entry<Character, Long>>(unsortMap.entrySet());
 
		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<Character, Long>>() {
			public int compare(Map.Entry<Character,  Long> o1,
                                           Map.Entry<Character,  Long> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
 
		// Convert sorted map back to a Map
		Map<Character,  Long> sortedMap = new LinkedHashMap<Character, Long>();
		for (Iterator<Map.Entry<Character,  Long>> it = list.iterator(); it.hasNext();) {
			Map.Entry<Character, Long> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	// SimpleDateFormat is not thread-safe, so give one to each thread
    private static final ThreadLocal<SimpleDateFormat> formatter = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue()
        {
            return new SimpleDateFormat("yyyyMMdd_HHmmss.SSS");
        }
        
    };
	
    
    public static void checkCredits() throws RemoteException {
    	  synchronized(admins){
    	   for(Admin a: admins){
    	    synchronized(loggedInUsers){
    	     for(User u  : loggedInUsers.values()){
    	      a.checkCredits(u.getName(), u.getCredits());
    	     }
    	    }
    	   }
    	  }
    	 }

}
