package myClasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Queue;
import java.util.concurrent.Callable;

import client.EncryptionManager;
import controller.CloudController;

public class CloudControllerThreadForNode implements Callable<String> {
	
	private Socket socket;
	private String adress;
	private int port;
	private String [] term;
	private EncryptionManager engManager;
	
	public CloudControllerThreadForNode(int port, String adress, String [] term, EncryptionManager manager) {
		this.port = port;
		this.adress = adress;
		this.term = term;
		this.engManager = manager;
	}

	@Override
	public String call() throws Exception {
		// TODO Auto-generated method stub
		String in = "";	
		// TODO Auto-generated method stub
		
				try {
					
					socket = new Socket(adress,port);
					
						//BufferedOutputStream outToNode = new BufferedOutputStream(
							//	new DataOutputStream(
								//		socket
									//			.getOutputStream()));
						
						PrintStream outToNode = new PrintStream(socket.getOutputStream());
						 
						
						BufferedReader inFromNode = new BufferedReader(
								new InputStreamReader(
										socket
												.getInputStream()));
					
					
						/*System.out.println("Ein paar Angaben ..." + socket.getPort() + " "
								+ socket.getLocalPort() + " " + socket.getInetAddress()
								+ " " + socket.getLocalSocketAddress());*/

						String writeOut = "!compute" + " " + term[0] + " " + term[1]
								+ " " + term[2];
						
						engManager.doHMAC(writeOut);
						
						//System.out.println(writeOut);
						
						outToNode.println(writeOut);

						//System.out.println("fertig -----");

						in = inFromNode.readLine();
						//System.out.println(in);
						
					
					
					inFromNode.close();
					outToNode.close();
					socket.close();
					//System.out.println("alles geschlossen");
					
					//return null;

				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					in = "Error";
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					in = "Error";
				}
				
				return in;
	}

}
