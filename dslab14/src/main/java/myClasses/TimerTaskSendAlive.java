package myClasses;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import controller.CloudController;
import node.Node;

public class TimerTaskSendAlive extends TimerTask{
	
private DatagramSocket udpSocket;
private int tcpPort;
private String inetAdress;
private int udpPort;
private int rmin;
//map beinhaltet die ports und die ueber tcp zurueckgegebenen status ok oder nok
public static Map<Integer, String> nodeHandling = new HashMap<Integer, String>();
private ExecutorService service;
private Future<String> task;
	
	public TimerTaskSendAlive(DatagramSocket udpSocket, int tcpPort, String inetAdress, int udpPort, int rmin){
		this.udpSocket = udpSocket;
		this.tcpPort =  tcpPort;
		this.inetAdress = inetAdress;
		this.udpPort = udpPort;
		this.rmin = rmin;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {	
			
			//beginne mit kontaktaufnahme mit Controller durch hello
			if (Node.helloMessage == false) {
				
				String in = "";
				String data = "!hello" + " " + rmin;
				byte[] buffer = data.getBytes();
				//System.out.println("Send: " + data);
				udpSocket.send(new DatagramPacket(buffer, buffer.length,
						InetAddress.getByName(inetAdress), udpPort));
				//System.out.println("gesendet ----");

				DatagramPacket packet = new DatagramPacket(new byte[1024],
						1024);
				udpSocket.receive(packet);

				//System.out.println("Received: " + new String(packet.getData()));
				
				Node.helloMessage = true;

				if (new String(packet.getData()).contains("OK_First")) {
					Node.firstNode = true;
					in = new String(packet.getData());
					System.out.println(in);
					String [] split = in.split(" ");
					if(split.length == 2) {
						if(Integer.parseInt(split[1].trim()) > Node.rmin) {
							Node.resource = split[1].trim();
							Node.oldResource = Node.resource;
						} else {
							System.out.println("Can't join cloud, shutting down!");
						}
					}
				} else if(new String(packet.getData()).contains("NOK_First")) {
					System.out.println("Can't join cloud, shutting down!");
				} else if(new String(packet.getData()).startsWith("!init")) {
					in = new String(packet.getData());
					String [] split = in.split(" ");
					int calculation = 0;
					String error = "";
					String answere = "";
					String order = "";
					
					//split - 2 da der befehl init und die letze stelle rmax nicht zaehlen
					if(split.length-2 > 0) {
						//System.out.println("Array letzte Stelle: " + split[split.length-1].trim());
						calculation = Integer.parseInt(split[split.length-1].trim())/((split.length-2)+1);
					} else {
						error = "No valid Node-Connections send.";
					}
					
					for (int i = 1; i < split.length - 1; i++) {
					
						// erster kontakt an andere nodes
						order = "!first_share";
						service = Executors.newFixedThreadPool(1);
						task = service.submit(new NodeHandlerTCPTask(Integer
								.parseInt(split[i]), "localhost", calculation,
								order));

						answere = task.get();

						nodeHandling.put(Integer.parseInt(split[i]), answere);
						service.shutdown();
					}
					
					//error gefunden starten des rollback
					if(nodeHandling.containsValue("!nok")) {
						for (int i = 1; i < split.length - 1; i++) {
							// starte den rollback vorgang
							order = "!rollback";
							service = Executors.newFixedThreadPool(1);
							task = service.submit(new NodeHandlerTCPTask(
									Integer.parseInt(split[i]), "localhost",
									calculation, order));

							answere = task.get();

							if (nodeHandling.size() > 0) {
								nodeHandling.remove(Integer.parseInt(split[i]));
							}
							
							service.shutdown();
						}
						System.out
						.println("Can't join cloud, shutting down!");
					} else {
						//kein error somit commit an nodes schicken
						for (int i = 1; i < split.length - 1; i++) {
							// starte den rollback vorgang
							order = "!commit";
							service = Executors.newFixedThreadPool(1);
							task = service.submit(new NodeHandlerTCPTask(
									Integer.parseInt(split[i]), "localhost",
									calculation, order));

							answere = task.get();

							if (nodeHandling.size() > 0) {
								nodeHandling.remove(Integer.parseInt(split[i]));
							}
							
							service.shutdown();
							Node.aliveMessage = true;
							Node.resource = "" + calculation;
							Node.oldResource = Node.resource;
						}
					}
				}

			}
			
			
			//beginne mit senden der alive messages
			//wenn erste node dann true
			//sonst nur wenn alivemessage true
			if((Node.helloMessage == true && Node.aliveMessage == true) || (Node.firstNode == true)) {
				String s = "!alive" + " " + Node.getArguments() + " " + "12345"
						+ " " + Node.getComponentName() + " " + tcpPort + " "
						+ udpPort + " " + inetAdress;
				byte[] data = s.getBytes();
				DatagramPacket packet_send2 = new DatagramPacket(data,
						data.length, InetAddress.getByName(inetAdress), udpPort);
				udpSocket.send(packet_send2);	
			}
					
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("TimerTaskAlive: Fehler beim Senden des DatagramPackets");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

}
