package myClasses;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.concurrent.Callable;

import client.EncryptionManager;
import node.Node;
import controller.CloudController;

//public class NodeTCPThreadPool implements Runnable {
public class NodeTCPThreadPool implements Runnable {

	private ServerSocket tcpServerSocket;
	private BufferedReader inFromCloud;
	private BufferedOutputStream outToCloud;
	private Socket cloudControllerSocket;
	private int rmin = 0;
	private int flagNoLog = 0;
	private String directory;
	private EncryptionManager engManager;

	public NodeTCPThreadPool(ServerSocket server, int rmin, String directory,
			EncryptionManager engManager) {
		this.tcpServerSocket = server;
		this.rmin = rmin;
		this.directory = directory;
		this.engManager = engManager;
	}

	@Override
	public void run() {

		// TODO Auto-generated method stub
		String input = "";
		String output = "";
		String output1 = "";
		while (!Node.exit_node) {
			try {
				cloudControllerSocket = tcpServerSocket.accept();
				// CloudController.loggedInUsers.put("dummy_user", new User(
				// "dummy_user", clientSocket, 100, "online"));
				// DatagramSocket datagramSocket = new DatagramSocket();

				/*
				 * System.out.println("Ein paar Angaben zur Node: " +
				 * cloudControllerSocket.getPort() + " " +
				 * cloudControllerSocket.getLocalPort() + " " +
				 * cloudControllerSocket.getInetAddress() + " " +
				 * cloudControllerSocket.getLocalSocketAddress());
				 */

				Node.nodeThreadPoolTCP.execute(new NodeTCPThreadPool(
						tcpServerSocket, rmin, directory, engManager));

				inFromCloud = new BufferedReader(new InputStreamReader(
						cloudControllerSocket.getInputStream()));
				outToCloud = new BufferedOutputStream(new DataOutputStream(
						cloudControllerSocket.getOutputStream()));

				String in;
				String[] split;

				// System.out.println("bereit zur eingabe");

				// System.out.println(inFromCloud.readLine());

				while (Node.exit_node == false
						&& (in = inFromCloud.readLine()) != null) {

					input = in;
					// System.out.println("hallo");
					System.out.println(in);
					split = in.split(" ");
					String out = "";
					boolean hmacOK = false;

					flagNoLog = 0;

					if (split[1].equals("!compute")) {

						if (split.length == 5) {

							try {
								hmacOK = engManager.doHMACResponse(in);
							} catch (InvalidKeyException
									| NoSuchAlgorithmException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {
							hmacOK = false;
						}
						// System.out.println(hmacOK);

						int result = 0;
						if (hmacOK) {
							if (split[2].matches("-?[0-9]+")
									&& split[4].matches("-?[0-9]+")) {
								if (split[3].equals("+")) {
									result = Integer.parseInt(split[2])
											+ Integer.parseInt(split[4]);
									out = result + "\n";
								} else if (split[3].equals("*")) {
									result = Integer.parseInt(split[2])
											* Integer.parseInt(split[4]);
									out = result + "\n";
								} else if (split[3].equals("/")) {
									// check ob zweite zahl 0
									if (Integer.parseInt(split[4]) == 0) {
										out = "Error: Division by Zero is not allowed."
												+ "\n";
									} else {
										result = (int) Math.round(Double
												.parseDouble(split[2])
												/ Double.parseDouble(split[4]));
										out = result + "\n";
									}
								} else if (split[3].equals("-")) {
									result = Integer.parseInt(split[2])
											- Integer.parseInt(split[4]);
									out = result + "\n";
								} else {
									out = "No mathematical expression." + "\n";
								}
							} else {
								out = "No mathematical expression." + "\n";
							}
						} else {
							out = "HMAC or computed Term does not match!"
									+ "\n";
						}
						outToCloud.write(out.getBytes());
						outToCloud.flush();
						output = out;
						if (Node.finishedExpressions.size() > 0) {
							int counter = Node.finishedExpressions.size();
							Node.finishedExpressions.put(counter + 1,
									"Inputvalue: " + input + " Outputvalue: "
											+ output);
						} else {
							Node.finishedExpressions.put(1, "Inputvalue: "
									+ input + " Outputvalue: " + output);
						}

					} else if (split[0].equals("!share")) {
						flagNoLog = 1;
						if (split.length < 2) {
							out = "!nok" + "\n";
						} else {

							if (Integer.parseInt(split[1]) < rmin) {
								out = "!nok" + "\n";
							} else {
								Node.sharedResource = split[1];
								out = "!ok" + "\n";
							}
						}
						//System.out.println(out);
						outToCloud.write(out.getBytes());
						outToCloud.flush();
						//System.out.println("Sender Node gesendet -----");

					} else if (split[0].equals("!rollback")) {
						flagNoLog = 1;
						Node.sharedResource = "0";

					} else if (split[0].equals("!commit")) {
						flagNoLog = 1;
						if (split.length == 2) {
							if(Node.sharedResource.equals(split[1])) {
								Node.oldResource = Node.resource;
								Node.resource = split[1];
								Node.sharedResource = "0";
							}
						}

						//System.out.println("commit fertig -----");

					} else if (split[0].equals("!resourceReset")) {
						// setze ressource auf die alte zurueck
						flagNoLog = 1;
						Node.resource = Node.oldResource;

					} else if (split[0].equals("!resourceResettoInitial")) {
						// setze ressource auf die initiale der cloud zurueck
						flagNoLog = 1;
						System.out.println("letzte nodeeee");
						if (split.length == 2) {
							Node.oldResource = Node.resource;
							Node.resource = split[1];
						}
					} else if (split[0].equals("!getLogs")) {

						File[] files = new File(directory).listFiles();
						// System.out.println("testtest" + files.length);
						for (File f : files) {
							output1 += f.getName();
							BufferedReader br = new BufferedReader(
									new FileReader(f));
							output1 += " " + br.readLine() + " = "
									+ br.readLine() + "\n";

						}
						output1 += "EOF" + "\n";
						// System.out.println(output1);
						outToCloud.write(output1.getBytes());
						outToCloud.flush();

					} else {
						flagNoLog = 1;
						out = "No such command!" + "\n";
						outToCloud.write(out.getBytes());
						outToCloud.flush();
					}

				}

			} catch (SocketException e) {
				// System.out.println("NodeTCPThreadPool closed");
			} catch (IOException e) {
				// System.out.println("ServerThread: Fehler beim Lesen/Schreiben vom/zum Client");
			} catch (NumberFormatException e) {
				String out = "No mathematical expression." + "\n";

				try {
					outToCloud.write(out.getBytes());
					outToCloud.flush();
					output = out;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} catch (ArithmeticException e) {
				String out = "Error: Division by Zero is not allowed." + "\n";

				try {
					outToCloud.write(out.getBytes());
					outToCloud.flush();
					output = out;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} finally {
				if (cloudControllerSocket != null && flagNoLog == 0) {
					try {
						cloudControllerSocket.close();
						inFromCloud.close();
						outToCloud.close();
						// log erstellen und schreiben
						// System.out.println(input);
						// System.out.println(output);
						if (!input.contains("!getLogs")) {
							if (Node.exit_node == false) {
								synchronized (input) {
									synchronized (output) {
										System.out.println(Node.loggerFacility(
												Node.getComponentName(), input,
												output));
									}
								}
							}
						}
						// System.out.println("schliese NodeTCPThreadPool");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}

	}

}
