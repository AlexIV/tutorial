package myClasses;

import java.net.Socket;

public class User {
	private String name;
	private Socket socket;
	private int credits;
	private String status;

	/**
	 * 
	 * @param name
	 * @param adresse
	 * @param tcpPort
	 * @param udpPort
	 */
	public User(String name, Socket socket, int credits, String status) {
		this.name = name;
		this.socket = socket;
		this.credits = credits;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String stat) {
		this.status = stat;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Socket getSocket() {
		return socket;
	}
	
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int cred) {
		this.credits = cred;
	}

}
