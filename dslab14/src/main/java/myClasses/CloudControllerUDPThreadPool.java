package myClasses;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;

import node.Node;
import controller.CloudController;

public class CloudControllerUDPThreadPool implements Runnable {
	
	private DatagramSocket socket;

	public CloudControllerUDPThreadPool(DatagramSocket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		//DatagramSocket socket;
				try {
					System.out.println("UDP wird abgehoert");
			while (CloudController.exit1 == false) {
				String[] split;
				String in = "";
				// Auf Anfrage warten
				DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
				socket.receive(packet);

				// Empfaenger auslesen
				int len = packet.getLength();
				byte[] data = packet.getData();
				
				InetAddress address = packet.getAddress();
		        int port = packet.getPort();
		   
				//System.out.println(new String(data, 0, len));

				// string gesendet: String s = "!alive" + " " +
				// Node.getArguments() + " " + "12345" + " " +
				// Node.getComponentName() + " " + tcpSocket + " " + udpPort;
				in = new String(data, 0, len);
				split = in.split(" ");

				
				if (split[0].equals("!alive")) {
					if (!CloudController.handeltNodes.containsKey(split[3])) {
						// neuer servicenode
						// ServiceNode (String name, Date end, String arguments,
						// String status, Socket socket, int port)
						//System.out.println("add Service Node");
						//usage ist am anfang 0
						CloudController.handeltNodes.put(
								split[3],
								new ServiceNode(split[3], new Date(), split[1],
										"online", Integer.parseInt(split[4]), Integer
												.parseInt(split[5]),split[6], 0));
					} else {
						//servicenode existoert bereits -> Date updaten
						//System.out.println("update Service Node");
						//schauen ob auf offline
						if(CloudController.handeltNodes.get(split[3]).getStatus().equals("offline")){
							CloudController.handeltNodes.get(split[3]).setEnd(new Date());
							CloudController.handeltNodes.get(split[3]).setStatus("online");
						} else {
							CloudController.handeltNodes.get(split[3]).setEnd(new Date());
						}
						
					}
					
				} else if (split[0].equals("!hello")) {
					
					String send = "";
					if(CloudController.handeltNodes.size() == 0) {
						//send = "No other nodes found!";
						if(Integer.parseInt(split[1]) <= CloudController.rmax) {
							send = "OK_First";
						} else {
							send = "NOK_First";
						}
						send = send + " " +  CloudController.rmax;
						
					} else {
						send = "!init ";
						for(ServiceNode node: CloudController.handeltNodes.values()) {
							if(node.getStatus().equals("online")) {
								send = send + node.getSocket() + " ";
							}
						}
						send = send + CloudController.rmax;
					}
					//alle nodes offline und node geht wieder online
					if(send.split(" ").length == 2) {
						send = "OK_First" + " " +  CloudController.rmax;
					}
					
					System.out.println(send);
					byte[] firstData = send.getBytes();
					
					packet  = new DatagramPacket(firstData, firstData.length);
					packet.setAddress(address);
					packet.setPort(port);
							//InetAddress.getByName(split[2]), Integer.parseInt(split[1]));
					socket.send(packet);
					//System.out.println("Controller gesendet -----");
					
				}

			}
				} catch (SocketException e1) {
					System.out.println("CloudControllerUDPThreadPool shutdown");//Fehler beim Empfangen von DatagramPacket");
				}
				catch (IOException e) {
					System.out.println("CloudControllerUDPThreadPool shutdown");//Fehler beim Empfangen von DatagramPacket");
				}
		
	}

}
