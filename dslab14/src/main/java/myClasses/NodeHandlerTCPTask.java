package myClasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;

public class NodeHandlerTCPTask implements Callable<String> {
	
	private int port;
	private String host;
	private int sharedNumber;
	private Socket socket;
	private String order;
	
	public NodeHandlerTCPTask(int port, String host, int sharedNumber, String order) {
		this.port = port;
		this.sharedNumber = sharedNumber;
		this.host = host;
		this.order = order;
		
	}

	@Override
	public String call() throws Exception {
		String in = "";	
		try {
			//System.out.println(port);
			//System.out.println(host);
			socket = new Socket(host, port);

			PrintStream outToNode = new PrintStream(socket.getOutputStream());

			BufferedReader inFromNode = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));

			/*
			 * System.out.println("Ein paar Angaben: " + socket.getPort() + " "
			 * + socket.getLocalPort() + " " + socket.getInetAddress() + " " +
			 * socket.getLocalSocketAddress());
			 */
			if (order.equals("!first_share")) {
				
				String writeOut = "!share" + " " + sharedNumber;
				outToNode.println(writeOut);
				in = inFromNode.readLine();
				
			} else if (order.equals("!commit")) {
				
				String writeOut = "!commit" + " " + sharedNumber;
				outToNode.println(writeOut);
				in = "commit ok";
				
			} else if (order.equals("!rollback")) {
				
				String writeOut = "!rollback";
				outToNode.println(writeOut);
				in = "rollback ok";
				
			} else if (order.equals("!resourceReset")) {
				
				String writeOut = "!resourceReset";
				outToNode.println(writeOut);
				in = "resourceReset ok";
				
			} else if (order.equals("!resourceResettoInitial")) {
				
				String writeOut = "!resourceResettoInitial" + " " + sharedNumber;
				outToNode.println(writeOut);
				in = "resourceResettoInitial ok";
			}

			inFromNode.close();
			outToNode.close();
			socket.close();

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			in = "Error";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			in = "Error";
		}
		return in;
		
	}

}
