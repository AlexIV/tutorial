package myClasses;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import util.Config;
import admin.INotificationCallback;
import client.EncryptionManager;
import controller.CloudController;
import controller.ControllerClientConnection;

public class CloudControllerThreadPool implements Runnable {

	private final ServerSocket tcpServerSocket;
	private final Config config;
	private final Config realConfig;
	private ExecutorService service;
	private Future<String> task;
	private int plus;
	private int minus;
	private int div;
	private int mal;
	private Map<String, Integer> subscribes;
	private INotificationCallback callback;
	private EncryptionManager engManager;

	public CloudControllerThreadPool(ServerSocket server, Config config,
			Config realConfig) {
		this.tcpServerSocket = server;
		this.config = config;
		this.realConfig = realConfig;

	}

	public int getPlus() {
		return plus;
	}

	public int getMinus() {
		return minus;
	}

	public int getDiv() {
		return div;
	}

	public int getMal() {
		return mal;
	}

	public Map<String, Integer> getSubscribes() {
		return subscribes;
	}

	public void addSubscribes(String name, int zahl,
			INotificationCallback callback) {

		if (subscribes == null) {
			subscribes = new HashMap<String, Integer>();
		}

		if (subscribes.containsKey(name)) {
			subscribes.remove(name);
			subscribes.put(name, zahl);
		} else {

			subscribes.put(name, zahl);
		}
		if (this.callback == null) {
			this.callback = callback;
		}
		// System.out.println(subscribes);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		// System.out.println(subscribes);
		boolean isTrue = true;
		Socket clientSocket;
		while (isTrue && !CloudController.exit1) {
			try {

				clientSocket = tcpServerSocket.accept();
				// CloudController.loggedInUsers.put("dummy_user", new User(
				// "dummy_user", clientSocket, 100, "online"));
				// DatagramSocket datagramSocket = new DatagramSocket();

				CloudController.threadPoolTCP
						.execute(new CloudControllerThreadPool(tcpServerSocket,
								config, realConfig));

				BufferedReader inFromClient = new BufferedReader(
						new InputStreamReader(clientSocket.getInputStream()));
				BufferedOutputStream outToClient = new BufferedOutputStream(
						new DataOutputStream(clientSocket.getOutputStream()));
				/*
				 * System.out.println(clientSocket.getPort() + " " +
				 * clientSocket.getLocalPort() + " " +
				 * clientSocket.getInetAddress() + " " +
				 * clientSocket.getLocalSocketAddress());
				 */

				if (CloudController.exit1) {
					clientSocket.close();
				}
				ControllerClientConnection ccc = new ControllerClientConnection(
						realConfig);

				String in;
				String[] split;
				boolean loggedIn = false;
				while (isTrue == true && CloudController.exit1 == false
						&& (in = inFromClient.readLine()) != null) {
					split = new String[10];
					if (loggedIn) {
						in = new String(ccc.decrypt(in), "UTF-8");
					}
					split = in.split(" ");
					// System.out.println(in);
					if (!loggedIn && !split[0].equals("!exit")) {
						loggedIn = false;
						// System.out.println(in);

						if (!loggedIn) {

							try {

								try {// login handshake
									String msg2 = ccc.loginHandshakeMsg1(in);
									String msg2E = new String(msg2) + "\n";
									outToClient.write(msg2E.getBytes());
									outToClient.flush();
									in = inFromClient.readLine();
									ccc.loginHandshakeMsg3(in);

								} catch (NoSuchAlgorithmException e) {
									System.out.println(e.getMessage());
								}
								if (ccc.isValid()) {
									//String match_credit = ccc.getUsername()
										//	+ ".credits";
									
									//Update Florian es sind schon alle User 
									//vorhanden und auf dem status offline
									if (!CloudController.loggedInUsers
											.containsKey(ccc.getUsername())) {
										//fehler -> user gibt es nicht
										String out = "User " + " does not exist."
												+ "\n";
										outToClient.write(out.getBytes());
										outToClient.flush();
										/*CloudController.loggedInUsers
												.put(ccc.getUsername(),
														new User(
																ccc.getUsername(),
																clientSocket,
																config.getInt(match_credit),
																"online"));*/
									} else {
										// user gibt es schon man muss nur
										// status aendern
										//socket setzen
										CloudController.loggedInUsers.get(ccc.getUsername()).setStatus("online");
										CloudController.loggedInUsers.get(ccc.getUsername()).setSocket(clientSocket);
									}
									loggedIn = true;
								}		

							} catch (MissingResourceException e) {
								// System.out.println("User does not exist!");
								String out = "User " + " does not exist."
										+ "\n";
								outToClient.write(out.getBytes());
								outToClient.flush();
							}
						}
					} else if (split[0].equals("!logout")) {
						synchronized (CloudController.loggedInUsers) {
							// System.out.println(in);
							if (split.length > 1) {
								if (CloudController.loggedInUsers
										.containsKey(split[1])
										&& CloudController.loggedInUsers
												.get(split[1]).getStatus()
												.equals("online")) {

									String out = "successfully logged out as "
											+ split[1] + "\n";
									CloudController.loggedInUsers.get(split[1])
											.setStatus("offline");
									outToClient.write((ccc.encrypt(out) + "\n")
											.getBytes());
									outToClient.flush();
									loggedIn = false;
								} else {
									String out = "you have to log in first";
									outToClient.write((ccc.encrypt(out) + "\n")
											.getBytes());
									outToClient.flush();
								}
							} else {
								String out = "you have to log in first";
								outToClient.write((ccc.encrypt(out) + "\n")
										.getBytes());
								outToClient.flush();
							}
						}

					} else if (split[0].equals("!credits")) {
						synchronized (CloudController.loggedInUsers) {
							if (split.length == 2) {
								if (CloudController.loggedInUsers
										.containsKey(split[1])
										&& CloudController.loggedInUsers
												.get(split[1]).getStatus()
												.equals("online")) {

									String out = "You have credits "
											+ CloudController.loggedInUsers
													.get(split[1]).getCredits()
											+ " left.";

									outToClient.write((ccc.encrypt(out) + "\n")
											.getBytes());
									outToClient.flush();
								} else {
									String out = "you have to log in first";
									outToClient.write((ccc.encrypt(out) + "\n")
											.getBytes());
									outToClient.flush();
								}
							} else {
								String out = "you have to log in first";
								outToClient.write((ccc.encrypt(out) + "\n")
										.getBytes());
								outToClient.flush();
							}
						}

					} else if (split[0].equals("!list")) {
						synchronized (CloudController.loggedInUsers) {
							synchronized (CloudController.handeltNodes) {
								if (split.length == 2) {
									String out = "";
									if (CloudController.loggedInUsers
											.containsKey(split[1])
											&& CloudController.loggedInUsers
													.get(split[1]).getStatus()
													.equals("online")) {
										if (CloudController.handeltNodes.size() > 0) {
											String op = "";
											for (ServiceNode node : CloudController.handeltNodes
													.values()) {
												if (node.getStatus().equals(
														"online")) {
													op = op + node.gerArgs();
													for (char c : op
															.toCharArray()) {
														if (!out.contains(c
																+ "")) {
															out += c;
														}
													}

												}
											}
											if (out == "") {
												out = "There is no service online!";
											}
											outToClient
													.write((ccc.encrypt(out) + "\n")
															.getBytes());
											outToClient.flush();

										} else {
											out = "At the moment there are no services available.";
											outToClient
													.write((ccc.encrypt(out) + "\n")
															.getBytes());
											outToClient.flush();
										}
									} else {
										String ou1t = "you have to log in first";
										outToClient
												.write((ccc.encrypt(ou1t) + "\n")
														.getBytes());
										outToClient.flush();
									}
								} else {
									String out = "you have to log in first";
									outToClient.write((ccc.encrypt(out) + "\n")
											.getBytes());
									outToClient.flush();
								}
							}
						}

					} else if (split[0].equals("!compute")) {
						String error = "";
						String getSocket = "";
						int port = 0;
						String outFromNode = "";
						String out = "";
						String nodeName = "";
						int operationCount = 0;
						String sendToNode = "";
						// checkliste erstellen welche am ende alles managet ->
						// usage und credits setzen wenn kein fehler
						Map<String, String> managedList = new HashMap<String, String>();

						synchronized (CloudController.loggedInUsers) {
							// System.out.println("error am anfang: " + error);
							// !compute 5 + 5 alice

							if (CloudController.loggedInUsers
									.containsKey(split[split.length - 1])
									&& CloudController.loggedInUsers
											.get(split[split.length - 1])
											.getStatus().equals("online")) {

								int termCount = 0;
								String[] term = new String[3];
								String result = "";
								// 5 + 5 + 5 + 5 alice
								// System.out.println("laenge von split "
								// + split.length);
								for (int i = 1; i < split.length - 1; i++) {
									// System.out.println(termCount);
									// System.out.println(i);
									// System.out.println("das ist term: " +
									// term[0] + term[1] + term[2]);
									// achtung setze ergenis ein auf erste
									// stelle
									if (!outFromNode.equals("")
											&& termCount == 0) {
										term[termCount] = outFromNode;
										termCount++;
										term[termCount] = split[i];
									} else {
										term[termCount] = split[i];
									}
									termCount++;
									if (termCount == 3) {
										termCount = 0;
										/*
										 * System.out
										 * .println("das ist term voll: " +
										 * term[0] + term[1] + term[2]);
										 */
										// ueberpruefe ob das vorzeichen
										// vorkommt ist eines angegeben
										if (term[1].matches("[+*/-]")) {
											// RMI
											if (term[1].equals("+")) {
												plus = plus + 1;

											}
											if (term[1].equals("/")) {
												div = div + 1;
											}
											if (term[1].equals("-")) {
												minus = minus + 1;
											}
											if (term[1].equals("*")) {
												mal = mal + 1;
											}
											// RMI
											int usage = 0;
											int counter = 0;
											// check ist ueberhaupt er service
											// zur bearbeitung vorhanden
											synchronized (CloudController.handeltNodes) {
												// synchronized (usage) {
												if (CloudController.handeltNodes
														.size() > 0) {
													for (ServiceNode node : CloudController.handeltNodes
															.values()) {

														if (node.gerArgs()
																.contains(
																		term[1])) {

															// initialisiere
															// usage
															if (counter != 1
																	&& node.getStatus()
																			.equals("online")) {
																usage = node
																		.getUsage();
																counter++;
															}

															// finde node mit
															// minimalen Usage
															// und
															// merke port und
															// socket
															// System.out
															// .println("usage vergleich: "
															// + usage);

															if (node.getUsage() <= usage
																	&& node.getStatus()
																			.equals("online")) {
																port = node
																		.getSocket();
																getSocket = node
																		.getAdress();
																nodeName = node
																		.getName();
																// System.out
																// .println("Usage: "
																// +
																// node.getUsage()
																// + " Name: " +
																// node.getName());
															}
														}
													}
												}
											}
											if (port != 0
													&& !getSocket.equals("")) {

												// System.out
												// .println("starte callable");

												service = Executors
														.newFixedThreadPool(1);
												engManager = new EncryptionManager(
														realConfig);
												synchronized (service) {
													try {
														// sendToNode =
														// engManager.doHMAC(term);
														task = service
																.submit(new CloudControllerThreadForNode(
																		port,
																		getSocket,
																		term,
																		engManager));

														outFromNode = task
																.get();
														// System.out
														// .println("Aufgabe node rueckgabe: "
														// + outFromNode);
														if (!outFromNode
																.matches("-?[0-9]+")) {

															managedList
																	.put("error",
																			"Service calculation error. Node return value: "
																					+ outFromNode);
															// error = "X";
														} else {
															// error = "";
															operationCount++;
															if (Integer
																	.parseInt(outFromNode) < 0) {
																managedList
																		.put("Operation "
																				+ operationCount,
																				"Result:"
																						+ " "
																						+ outFromNode
																						+ " "
																						+ "NodeName:"
																						+ " "
																						+ nodeName
																						+ " "
																						+ "Usage:"
																						+ " "
																						+ 50
																						* (outFromNode
																								.length() - 1)
																						+ " "
																						+ "User:"
																						+ " "
																						+ split[split.length - 1]);
															} else {
																managedList
																		.put("Operation "
																				+ operationCount,
																				"Result:"
																						+ " "
																						+ outFromNode
																						+ " "
																						+ "NodeName:"
																						+ " "
																						+ nodeName
																						+ " "
																						+ "Usage:"
																						+ " "
																						+ 50
																						* outFromNode
																								.length()
																						+ " "
																						+ "User:"
																						+ " "
																						+ split[split.length - 1]);
															}

															/*
															 * System.out
															 * .println
															 * ("Result:" + " "
															 * + outFromNode +
															 * " " +"NodeName:"
															 * +" " +nodeName
															 * +" " + "Usage:" +
															 * " " + 50
															 * outFromNode
															 * .length() + " " +
															 * "User:" + " " +
															 * split
															 * [split.length -
															 * 1]);
															 */

														}
														service.shutdown();

													} catch (InterruptedException e) {
														// TODO Auto-generated
														// catch block
														managedList.put(
																"error",
																e.toString());
													} catch (ExecutionException e) {
														// TODO Auto-generated
														// catch block
														managedList.put(
																"error",
																e.toString());
													}

													term[0] = "";
													term[1] = "";
													term[2] = "";
												}
												// System.out
												// .println("-----------erste runde");
											} else {
												// port oder socketadresse nicht
												// zugeteilt
												managedList
														.put("error",
																"No port or socket adress found. Please verify that all needed services are available.");
												// error = "X";
											}

										} else {
											// kein opartor gefunden
											managedList
													.put("error",
															"No mathematical operator was found. Please verify your mathematical statement.");
											// error = "X";
										}
									}

								}
								/*
								 * System.out.println("kurz vor ausgabe");
								 * 
								 * System.out.println("Out from Node: " +
								 * outFromNode);
								 */
								synchronized (managedList) {

									if (!managedList.containsKey("error")) {

										// checke ob user die
										// anfrage bezahlen kann
										synchronized (CloudController.loggedInUsers) {
											if (CloudController.loggedInUsers
													.containsKey(split[split.length - 1])
													&& CloudController.loggedInUsers
															.get(split[split.length - 1])
															.getStatus()
															.equals("online")) {

												if (CloudController.loggedInUsers
														.get(split[split.length - 1])
														.getCredits()
														- 50
														* managedList.size() >= 0) {
													CloudController.loggedInUsers
															.get(split[split.length - 1])
															.setCredits(
																	CloudController.loggedInUsers
																			.get(split[split.length - 1])
																			.getCredits()
																			- 50
																			* managedList
																					.size());

													// System.out.println(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getName());
													// System.out.println(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getCredits());
													// System.out.println(subscribes);
													// System.out.println(subscribes.get(CloudController.loggedInUsers.get(split[split.length
													// - 1])));
													// RMI
													// if(subscribes != null ){
													// if(subscribes.containsKey(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getName())){
													// if(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getCredits()
													// <subscribes.get(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getName())){
													// //
													// System.out.println("Test");
													// //callback.notify(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getName(),
													// subscribes.get(CloudController.loggedInUsers.get(split[split.length
													// - 1]).getName()));
													// CloudController.this.check_subs();
													// //boolean cb =
													// UnicastRemoteObject.unexportObject(callback,
													// true);
													// }
													// }
													// }
													// RMI

													// System.out
													// .println(CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getName());
													// System.out
													// .println(CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getCredits());
													// System.out
													// .println(subscribes);
													// System.out.println(subscribes.get(CloudController.loggedInUsers.get(split[split.length
													// - 1])));
													// RMI
													// if (subscribes != null) {
													// if (subscribes
													// .containsKey(CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getName())) {
													// if
													// (CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getCredits() <
													// subscribes
													// .get(CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getName())) {
													// //
													// System.out.println("Test");
													// callback.notify(
													// CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getName(),
													// subscribes
													// .get(CloudController.loggedInUsers
													// .get(split[split.length -
													// 1])
													// .getName()));
													// // boolean cb =
													// //
													// UnicastRemoteObject.unexportObject(callback,
													// // true);
													// }
													// }
													// }
													// RMI
													CloudController
															.checkCredits();

												} else {
													error = "X";
												}

											} else {
												error = "Y";
											}
										}

										synchronized (CloudController.handeltNodes) {
											// kann user bezahlen??
											if (error.equals("")) {
												for (String line : managedList
														.values()) {
													String[] splits = line
															.split(" ");
													// Result 5000 NodeName
													// node1
													// Usage 200 User alice
													// checke ob node noch immer
													// online und user noch
													// online
													// System.out
													// .println("zeile der map "
													// + line);
													if (CloudController.handeltNodes
															.containsKey(splits[3])
															&& CloudController.handeltNodes
																	.get(splits[3])
																	.getStatus()
																	.equals("online")
															&& CloudController.loggedInUsers
																	.containsKey(split[split.length - 1])
															&& CloudController.loggedInUsers
																	.get(split[split.length - 1])
																	.getStatus()
																	.equals("online")) {

														// setze neuen usage
														int oldUsage = CloudController.handeltNodes
																.get(splits[3])
																.getUsage();

														CloudController.handeltNodes
																.get(splits[3])
																.setUsage(
																		oldUsage
																				+ Integer
																						.parseInt(splits[5]));

													} else {
														error = "Y";
													}
												}
											} else {
												error = "X";
											}
										}

										if (error.equals("Y")) {
											out = "Error: Unexpected shutdown of user and/or node."
													+ "\n";
											outToClient
													.write((ccc.encrypt(out) + "\n")
															.getBytes());
											outToClient.flush();
										} else if (error.equals("X")) {
											out = "Error: You could not efford calculation. Please buy credits first."
													+ "\n";
											outToClient
													.write((ccc.encrypt(out) + "\n")
															.getBytes());
											outToClient.flush();
										} else {
											out = outFromNode;
											outToClient
													.write((ccc.encrypt(out) + "\n")
															.getBytes());
											outToClient.flush();
										}

									} else {
										// fehler aufgetreten baue error string
										for (String line : managedList.values()) {
											// System.out.println(line);
											if (!line.startsWith("Result:")) {
												out = out + line;
											}
										}

										outToClient
												.write((ccc.encrypt(out) + "\n")
														.getBytes());
										outToClient.flush();
										error = "";
									}
								}
							} else {
								out = "you have to log in first\n";
								outToClient.write((ccc.encrypt(out) + "\n")
										.getBytes());
								outToClient.flush();
							}
						}
					} else if (split[0].equals("!buy")) {
						synchronized (CloudController.loggedInUsers) {
							// System.out.println(split.toString());
							if (split.length == 3) {
								if (CloudController.loggedInUsers
										.containsKey(split[2])
										&& CloudController.loggedInUsers
												.get(split[2]).getStatus()
												.equals("online")) {
									if (split[1].matches("[0-9]+")) {
										int old_credits = 0;
										old_credits = old_credits
												+ CloudController.loggedInUsers
														.get(split[2])
														.getCredits();
										CloudController.loggedInUsers.get(
												split[2]).setCredits(
												Integer.parseInt(split[1])
														+ old_credits);
										String out = "You now have "
												+ CloudController.loggedInUsers
														.get(split[2])
														.getCredits()
												+ " credits." + "\n";
										outToClient
												.write((ccc.encrypt(out) + "\n")
														.getBytes());
										outToClient.flush();

									} else {
										String out = "Credits musst be an interger value.";
										outToClient
												.write((ccc.encrypt(out) + "\n")
														.getBytes());
										outToClient.flush();
									}
								} else {
									String out = "you have to log in first\n";
									outToClient.write((ccc.encrypt(out) + "\n")
											.getBytes());
									outToClient.flush();
								}
							} else {
								String out = "you have to log in first\n";
								outToClient.write((ccc.encrypt(out) + "\n")
										.getBytes());
								outToClient.flush();
							}
						}
					} else if (split[0].equals("!exit")) {
						if (split.length > 1) {
							if (CloudController.loggedInUsers
									.containsKey(split[1])) {
								CloudController.loggedInUsers.get(split[1])
										.setStatus("offline");
							}
							if (!split[1].equals("dummy")) {
								outToClient.write((ccc.encrypt("ok") + "\n")
										.getBytes());
								outToClient.flush();
							}
							isTrue = false;
							outToClient.close();
							inFromClient.close();
							// datagramSocket.close();

						}
					} else {
						String out = "command not found";
						outToClient.write((ccc.encrypt(out) + "\n").getBytes());
						outToClient.flush();
					}

				}

			} catch (SocketException e) {
				System.out.println("CloudControllerTCPThreadPool shutdown");// :
																			// Fehler
																			// beim
																			// Erstellen/Schlie�en
																			// eines
																			// Sockets");
			} catch (IOException e) {
				System.out
						.println("ServerThread: Fehler beim Lesen/Schreiben vom/zum Client"
								+ e);
			}

		}

	}
}
