package myClasses;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import controller.CloudController;
import node.Node;

//kontrolliert gesendete isalive meldungen der nodes

public class TimerTaskAlive extends TimerTask{
	
	private Date current;
	private ExecutorService service;
	private Future<String> task;
	private int countValues;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		current = new Date();
		int flagResourceReset = 0;
		String order = "";
		String answere = "";
		 
		
		for (ServiceNode nodes : CloudController.handeltNodes.values()) {
			//System.out.println(current.getTime());
			//System.out.println(current.getTime()-3000);
			//System.out.println(nodes.getEnd().getTime());
			if(nodes.getStatus().equals("online") && (nodes.getEnd().getTime() < (current.getTime()-3000))) {
				System.out.println(nodes.getName() + " auf offline gesetzt");
				nodes.setStatus("offline");
				flagResourceReset = 1;
				countValues++;
			} 
		}
		
		// alle nodes bis auf eine offline somit ressource auf initial status
		// von cloud setzen
		if ((CloudController.handeltNodes.size() - countValues == 1) && countValues > 0) {
			for (ServiceNode nodes : CloudController.handeltNodes.values()) {
				if (nodes.getStatus().equals("online")) {
					//System.out.println("achtung nur mehr eine node");
					order = "!resourceResettoInitial";
					service = Executors.newFixedThreadPool(1);
					task = service.submit(new NodeHandlerTCPTask(nodes
							.getSocket(), "localhost", CloudController.rmax,
							order));

					try {
						answere = task.get();
					} catch (InterruptedException | ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					service.shutdown();
				}
			}
			countValues = 0;
		} else {
			// eine node wurde beendet und ressourcen muessen neu verteilt
			// werden
			if (flagResourceReset == 1) {
				// suche online nodes und schiche nachricht zum setzen der
				// ressourcen
				for (ServiceNode nodes : CloudController.handeltNodes.values()) {
					if (nodes.getStatus().equals("online")) {

						order = "!resourceReset";
						service = Executors.newFixedThreadPool(1);
						task = service.submit(new NodeHandlerTCPTask(nodes
								.getSocket(), "localhost", 00, order));

						try {
							answere = task.get();
						} catch (InterruptedException | ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						service.shutdown();
					}
				}

			}
		}
	}

}
