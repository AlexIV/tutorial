package myClasses;

import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;

public class ServiceNode {

	private String name;
	private Date end;
	private String arguments;
	private String status;
	private int tcpPort;
	private int port;
	private String adress;
	private int usage;

	public ServiceNode(String name, Date end, String arguments, String status,
			int tcpPort, int port, String adress, int usage) {
		this.name = name;
		this.end = end;
		this.arguments = arguments;
		this.status = status;
		this.tcpPort = tcpPort;
		this.port = port;
		this.adress = adress;
		this.usage = usage;
	}

	public void setStatus(String stat) {
		this.status = stat;
	}

	public String getStatus() {
		return this.status;
	}

	public Date getEnd() {
		return this.end;
	}

	public String gerArgs() {
		return this.arguments;
	}

	public String getName() {
		return this.name;
	}

	public void setEnd(Date newEnd) {
		this.end = newEnd;
	}

	public int getSocket() {
		return this.tcpPort;
	}

	public int getPort() {
		return this.port;
	}

	public String getAdress() {
		return this.adress;
	}

	public void setUsage(int use) {
		this.usage = use;
	}

	public int getUsage() {
		return this.usage;
	}

}
