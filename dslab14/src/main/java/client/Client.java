package client;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import util.Config;

public class Client implements IClientCli, Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	public static boolean exit = false;
	public static String loggedInAs = "";

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Client(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;

		// TODO
	}

	@Override
	public void run() {
		// TODO

		/**
		 * Create Client TCP-Socket
		 */
		Socket client = null;

		try {
			client = new Socket(config.getString("controller.host"),
					config.getInt("controller.tcp.port"));

			BufferedReader inFromStdin = new BufferedReader(
					new InputStreamReader(userRequestStream));

			// create a reader to retrieve messages send by the server
			BufferedReader serverReader = new BufferedReader(
					new InputStreamReader(client.getInputStream()));

			String in = "";
			String[] split;
			String inFromServer = "";

			BufferedOutputStream outtoserver = new BufferedOutputStream(
					new DataOutputStream(client.getOutputStream()));

			EncryptionManager encManager = new EncryptionManager(config,
					serverReader, outtoserver);

			System.out.println("Client Online" + " " + client);
			System.out.println(client.getPort() + " " + client.getLocalPort()
					+ " " + client.getInetAddress() + " "
					+ client.getLocalSocketAddress());

			// ExecutorService listenThreadPool =
			// Executors.newCachedThreadPool();
			// listenThreadPool.execute(new ClientTCPListenThread(client));

			while (exit == false && (in = inFromStdin.readLine()) != null) {
				in.replaceFirst("\n", " ");
				split = in.split(" ");

				if (split[0].equals("!authenticate")) {
					if (loggedInAs == "") {
						if (split.length == 2) {

							encManager.doAuthenticate(split[1]);
							System.out.println("Authenticate finished");

							loggedInAs = split[1];

						} else {
							System.out
									.println("Invalid Arguments. Usage: !authenticate <username> ");
						}
					} else {
						System.out.println("you are already logged in as "
								+ loggedInAs);
					}
				} else if (split[0].equals("!logout")) {

					if (split.length == 1) {
						String out = in + " " + loggedInAs;
						String response = encManager.doRequest(out);
						System.out.println(response);
						loggedInAs = "";

					} else {
						System.out.println("Invalid Arguments. Usage: !logout");
					}

				} else if (split[0].equals("!list")) {

					if (split.length == 1) {
						String out = in + " " + loggedInAs;
						String response = encManager.doRequest(out);
						System.out.println(response);
					} else {
						System.out.println("Invalide Arguments. Usage: !list");
					}

				} else if (split[0].equals("!credits")) {

					if (split.length == 1) {
						String out = in + " " + loggedInAs;
						String response = encManager.doRequest(out);
						System.out.println(response);
					} else {
						System.out
								.println("Invalide Arguments. Usage: !credits");
					}

					/*
					 * } else if (split[0].equals("")) {
					 * 
					 * String out = "test"; outtoserver.write(out.getBytes());
					 * outtoserver.flush();
					 */

				} else if (split[0].equals("!compute")) {

					if (split.length >= 4) {

						String out = in + " " + loggedInAs;
						String response = encManager.doRequest(out);
						System.out.println(response);

					} else {
						System.out
								.println("Invalide Arguments. Usage: !compute <number> <operator> <number>");
					}

				} else if (split[0].equals("!buy")) {
					// System.out.println("ich bin im buy");
					if (split.length == 2) {
						String out = in + " " + loggedInAs;
						String response = encManager.doRequest(out);
						System.out.println(response);
					} else {
						System.out
								.println("Invalide Arguments. Usage: !buy <credits>");
					}

				} else if (split[0].equals("!exit")) {
					String out = split[0] + " " + loggedInAs;
					String response = "";
					if (loggedInAs.equals("")) {
						outtoserver
								.write((new String(split[0] + " dummy") + "\n")
										.getBytes());
						outtoserver.flush();
					} else {
						response = encManager.doRequest(out);
					}
					System.out.println(response);
					exit = true;
					outtoserver.close();
					serverReader.close();
					client.close();
					System.out.println("Client geschlossen");

				} else {
					System.out
							.println("Invalid Command. Use following commands instead:");
					System.out.println("!login <username> <password>");
					System.out.println("!logout");
					System.out.println("!list");
					System.out.println("!credits");
					System.out.println("!compute <mathematical expression>");
					System.out.println("!buy <credits>");
					System.out.println("!exit");
				}
			}

		} catch (UnknownHostException e) {
			System.err.println("Client closed"); // : Fehler beim Erstellen des
													// Client Sockets");
		} catch (FileNotFoundException e) {
			System.err.println("Client: User does not exist.");
		} catch (IOException e) {
			System.err.println("Client: IOException" + e);
		}

	}

	@Override
	public String login(String username, String password) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String logout() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String credits() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String buy(long credits) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String compute(String term) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String exit() throws IOException {
		// TODO Auto-generated method stub
		String out = "bitch";
		return out;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Client} component
	 */
	public static void main(String[] args) {
		Client client = new Client(args[0], new Config("client"), System.in,
				System.out);

		// TODO: start the client
		client.run();

	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String authenticate(String username) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
