package client;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;

import util.Config;
import util.CryptLib;
import util.Keys;

import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Mac;

public class EncryptionManager {

	private BufferedReader input;
	private BufferedOutputStream output;
	private Config config;
	private CryptLib crypt;

	public EncryptionManager(Config config, BufferedReader input,
			BufferedOutputStream output) {
		this.input = input;
		this.output = output;
		this.config = config;
		crypt = new CryptLib();
	}
	
	public EncryptionManager(Config config) {
		//this.input = input;
		//this.output = output;
		this.config = config;
		crypt = new CryptLib();
	}

	private byte[] createAuthMsg1(byte[] clientChallenge, String username)
			throws IOException {
		byte[] clientChallengeBase64 = Base64.encode(clientChallenge);
		PublicKey controllerKey = getControllerPublicKey();
		String msg = "!authenticate " + username + " "
				+ new String(clientChallengeBase64);

		return Base64.encode(crypt.encryptRSA(msg, controllerKey));
	}

	private byte[] createAuthMsg3(byte[] proxyChallengeBase64) {

		byte[] msg3 = crypt.cryptAES(proxyChallengeBase64, Cipher.ENCRYPT_MODE);
		return Base64.encode(msg3);
	}

	/**
	 * @return array elements may be base64 encoded
	 */
	private String[] checkAndReturnMsg2(byte[] msg2, PrivateKey userKey,
			String clientChallengeB64) throws InvalidObjectException {
		String decryptedMsg2 = crypt.decryptRSA(Base64.decode(msg2), userKey);
		String[] splitted = decryptedMsg2.split("\\s");
		if (!clientChallengeB64.equals(new String(splitted[1]))) {
			throw new InvalidObjectException("Controller is unknown!");
		}
		return splitted;
	}

	public boolean doAuthenticate(String username) throws IOException, FileNotFoundException {
		PrivateKey userKey = getPrivateUserKey(username);
		byte[] clientChallenge = crypt.generateSecureRandom(32);
		byte[] msg1 = createAuthMsg1(clientChallenge, username);

		output.write((new String(msg1 )+ "\n").getBytes());
		output.flush();
		String msg2 = (String) input.readLine();
		msg2=msg2.replace("\n", "");
		String[] decryptedMsg2 = checkAndReturnMsg2(msg2.getBytes(), userKey,
				new String(Base64.encode(clientChallenge)));
		// get secretKey and iv
		byte[] encodedSecretKey = Base64.decode(decryptedMsg2[3]);
		crypt.setSecretKey(new SecretKeySpec(encodedSecretKey, 0,
				encodedSecretKey.length, "AES"));
		crypt.setIv(new IvParameterSpec(Base64.decode(decryptedMsg2[4])));
		byte[] msg3 = createAuthMsg3(decryptedMsg2[2].getBytes());
		output.write((new String(msg3) + "\n").getBytes());
		output.flush();
		return true;
	}

	private PrivateKey getPrivateUserKey(String username) throws IOException, FileNotFoundException {
		String pathToPrivateKey = config.getString("keys.dir") + "/" + username
				+ ".pem";
		return Keys.readPrivatePEM(new File(pathToPrivateKey));

	}

	private PublicKey getControllerPublicKey() throws IOException {
		String path = config.getString("controller.key");
		PEMReader in = new PEMReader(new FileReader(path));
		return (PublicKey) in.readObject();
	}

	public String doRequest(String request) throws IOException {
		byte[] msg = Base64.encode(crypt.cryptAES(
				Base64.encode(request.getBytes()), Cipher.ENCRYPT_MODE));
		output.write((new String(msg)+"\n").getBytes());
		output.flush();
		String obj = input.readLine();
		byte[] decryptedB64 = crypt.cryptAES(Base64.decode(obj),
				Cipher.DECRYPT_MODE);
		byte[] response = Base64.decode(decryptedB64);
		return new String(response, "UTF-8");
	}
	
	private Key getHMACKey() throws IOException {
		String pathToHMACKey = config.getString("hmac.key");
		return Keys.readSecretKey(new File(pathToHMACKey));
	}
	
	public String doHMAC (String message) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
		Key secretKey = getHMACKey();
		Mac hMac = Mac.getInstance("HmacSHA256");
		hMac.init(secretKey);
		String test = new String(Base64.encode(hMac.doFinal(message.getBytes())));
		return test;
	}
	
	public String doHMACwithoutEncode (String message) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
		Key secretKey = getHMACKey();
		Mac hMac = Mac.getInstance("HmacSHA256");
		hMac.init(secretKey);
		String test = new String(hMac.doFinal(message.getBytes()));
		return test;
	}
	
	public boolean doHMACResponse(String message) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
		String [] split = message.split(" ");
		String plainText = split[1] + " " +split[2] + " " + split[3] + " " +split[4];
		byte[] encode = Base64.decode(split[0]);
		System.out.println(encode);
		String tests = new String(encode);
		System.out.println(tests);
		byte[] hmac = tests.getBytes();
		System.out.println("hmac: " + hmac);
		System.out.println(new String(hmac));
		String test2 = doHMACwithoutEncode(plainText);
		System.out.println("test2: " + test2);
		byte[] computedHash = test2.getBytes();
		//vergleiche beide
		boolean validHash = MessageDigest.isEqual(computedHash, hmac);
		return validHash;
	}
}
